package artificialFastqGenerator;

import java.util.ArrayList;

public abstract class AbstractRead {

	protected ArrayList<Nucleobase> sequence;
	
	/**
	 * Set the sequence of nucleobases from the reference genome which this Read object is a read of.
	 * 
	 * @param sequence
	 */
	
	public void setSequence(ArrayList<Nucleobase> sequence) {
		this.sequence=sequence;
	}

	/**
	 * Return the sequence of nucleobases from the reference genome which this Read object is a read of.
	 * 
	 * @return sequence - the sequence of nucleobases from the reference genome which this Read object is a read of.
	 */
	
	public ArrayList<Nucleobase> getSequence() {
		return this.sequence;
	}

}
