package artificialFastqGenerator;

import java.util.ArrayList;

/**
 * An instance of the Nucleobase class corresponds to a nucleobase in the human reference genome. It has fields for its
 * genotype, unique integer ID, target coverage, covering reads (i.e. those reads in which it is contained), and coverage. 
 * 
 * Copyright (C) 2012 The Institute of Cancer Research (ICR).
 *
 * This file is part of ArtificialFastqGenerator v1.0.0.
 * 
 * ArtificialFastqGenerator is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>
 * 
 * Authour's contact email: Matthew.Frampton@icr.ac.uk
 */

public class Nucleobase {
	
	private char genotype;
	private long uniqueID;
	private int targetCoverage;
	private ArrayList<Read> coveringReads;
	private int coverage;

	/**
	 * Initialise a new Nucleobase object.
	 * 
	 * @param genotype - the nucleobase's genotype.
	 * @param uniqueID - the nucleobase's unique integer ID.
	 * @param targetCoverage - the nucleobase's target coverage.
	 */
	
	public Nucleobase(char genotype, long uniqueID, int targetCoverage) {
		this.genotype = genotype;
		this.uniqueID = uniqueID;
		this.targetCoverage = targetCoverage;
		coveringReads = new ArrayList<Read>();
		coverage = 0;
	}
	
	/**
	 * Get the nucleobase's genotype.
	 * 
	 * @return genotype - the nucleobase's genotype.
	 */
	
	public char getGenotype() {
		return genotype;
	}
	
	/***
	 * GEt the complement of the nucleobase's genotype.
	 * 
	 * @return complementGenotype - the complement of the nucleobase's genotype.
	 */
	
	public char getComplementGenotype() {
		if (this.genotype == 'A') {
			return 'T';
		} else if (this.genotype == 'C') {
			return 'G';
		} else if (this.genotype == 'G') {
			return 'C';
		} else if (this.genotype == 'T') {
			return 'A';
		} else {
			return this.genotype;
		}
	}
	
	/**
	 * Set the nucleobase's genotype.
	 */
	
	public void setGenotype(char genotype) {
		this.genotype = genotype;
	}
	
	/**
	 * Add a read to the list of reads which cover this nucleobase.
	 * 
	 * @param read - the read which covers this nucleobase.
	 */
	
	public void addCoveringRead(Read read) {
		coveringReads.add(read);
		coverage = coverage + 1;
	}

	/**
	 * Get the coverage for this nucleobase i.e. the number of reads which include it.
	 * 
	 * @return coverage - coverage of the nucleobase.
	 */
	
	public int getCoverage() {
		return coverage;
	}
	
	/**
	 * Get the list of covering reads - the reads which include this nucleobase.
	 * 
	 * @return coveringReads - the ArrayList containing the reads which cover this nucleobase.
	 */
	
	public ArrayList<Read> getCoveringReads() {
		return coveringReads;
	}
	
	/**
	 * Get the nucleobase's unique integer ID.
	 * 
	 * @return uniqueID - the nucleobase's unique integer ID.
	 */
	
	public long getUniqueID() {
		return uniqueID;
	}
	
	/**
	 * Get the target coverage for this nucleobase (the number of reads that we want to 
	 * include this nucleobase).
	 * 
	 * @return targetCoverage - the nucleobase's target coverage.
	 */
	
	public int getTargetCoverage() {
		return targetCoverage;
	}
	
	/**
	 * Set the target coverage for this nucleobase (the number of reads we want to include
	 * this nucleobase).
	 * 
	 * @param targetCoverage - the nucleobase's target coverage.
	 */
	
	public void setTargetCoverage(int targetCoverage) {
		this.targetCoverage = targetCoverage;
	}
	
	/**
	 * Checks whether a nucleobase's coverage matches its target coverage.
	 * 
	 * @return coverageMatchesTarget - true if the coverage is >= than the target, else false.
	 */
	
	public boolean hasTargetCoverage() {
		
		boolean coverageMatchesTarget = false;
		if (this.getCoverage() >= this.getTargetCoverage()) {
			coverageMatchesTarget = true;}
		return coverageMatchesTarget;
		
	}
	
	public String toString() {
		if (Main.outputFormat.equals(Main.DEBUG_NUCLEOBASES_NUC_IDS)) {
			return genotype + ":" + this.uniqueID + " ";
		} else {
			return String.valueOf(genotype);
		}
	}
	
	
}
